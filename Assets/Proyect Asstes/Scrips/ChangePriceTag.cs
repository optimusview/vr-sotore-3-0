﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangePriceTag : MonoBehaviour {
    bool buttonAble = true;
    public GameObject panel;
    public int productNumber ;
	// Use this for initialization
	void Start () {



    }

    // Update is called once per frame
    void Update () {
		
	}

    void OnCollisionEnter(Collision col)
    {
        productNumber = 3;
        if (buttonAble)
        {

            if (col.gameObject.name == "UpButton")
            {
                Debug.Log("upbutton ");
                GlobalVariables.products[0] += 0.50f;
                EnableButton();

            }

            if (col.gameObject.name == "DownButton")
            {
                Debug.Log("downbutton");
                GlobalVariables.products[0] -= 0.50f;
                EnableButton();
            }

            /*
            if (col.gameObject.name == "chokis")
            {
                panel.SetActive(true);
            }
            */
        }

    }


    IEnumerator EnableButton()
    {
        buttonAble = false;
        yield return new WaitForSeconds(1);
        buttonAble = true;
    }

}
